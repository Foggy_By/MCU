
#include "key.h"
#include "delay.h"
#include "main.h"

void KEY_Config(void)
{
#if REG_CODE_EN == 1
	//1.PA0 PE2 PE3 PE4 的时钟开关
    RCC->AHB1ENR |= 1<<0|1<<4;

	//2.1GPIOA MODE ...
    GPIOA->MODER &= ~(3<<2*0);
    GPIOA->PUPDR &= ~(3<<2*0);

	//2.2GPIOE MODE ...
    GPIOE->MODER &= ~(3<<2*2);
    GPIOE->PUPDR &= ~(3<<2*2);

    GPIOE->MODER &= ~(3<<2*3);
    GPIOE->PUPDR &= ~(3<<2*3);

    GPIOE->MODER &= ~(3<<2*4);
    GPIOE->PUPDR &= ~(3<<2*4);
#else
    RCC_AHB1PeriphClockCmd(RCC_AHB1Periph_GPIOA|RCC_AHB1Periph_GPIOE,ENABLE);
	//定义结构体
	GPIO_InitTypeDef GPIO_InitStructure;
	//给结构体赋值
	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_0;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_High_Speed;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	//结构体参数生效
	GPIO_Init(GPIOA, &GPIO_InitStructure);

	GPIO_InitStructure.GPIO_Pin   = GPIO_Pin_2|GPIO_Pin_3|GPIO_Pin_4;
	GPIO_InitStructure.GPIO_Mode  = GPIO_Mode_IN;
	GPIO_InitStructure.GPIO_Speed = GPIO_High_Speed;
	GPIO_InitStructure.GPIO_OType = GPIO_OType_PP;
	GPIO_InitStructure.GPIO_PuPd  = GPIO_PuPd_NOPULL;
	GPIO_Init(GPIOE, &GPIO_InitStructure);

#endif

}


uint8_t KEY_Check(void)
{
	uint8_t val = 0;
	if(KEY1)
	{
		Delay_ms(10);
		if(KEY1)
		{
			while(KEY1);
			val = 1;
		}
	}
	else if(KEY2)
	{
		Delay_ms(10);
		if(KEY2)
		{
			while(KEY2);
			val = 2;
		}
	}
	else if(KEY3)
	{
		Delay_ms(10);
		if(KEY3)
		{
			while(KEY3);
			val = 3;
		}
	}
	else if(KEY4)
	{
		Delay_ms(10);
		if(KEY4)
		{
			while(KEY4);
			val = 4;
		}
	}
	return val;
}
