#ifndef _KEY_H
#define _KEY_H

#include "stm32f4xx.h"

//宏定义
#define KEY1 (GPIOA->IDR & (1<<0))//PA0输入电平状态，按下时为高电平
#define KEY2 (GPIOE->IDR & (1<<2))//PE2输入电平状态，按下时为低电平
#define KEY3 (GPIOE->IDR & (1<<3))//PE3输入电平状态，按下时为低电平
#define KEY4 (GPIOE->IDR & (1<<4))//PE4输入电平状态，按下时为低电平

//函数声明
void Key_Init(void);
u8 keyscan(void);


#endif

