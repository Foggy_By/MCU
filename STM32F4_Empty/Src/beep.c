#include "beep.h"
#include "delay.h"



void BEEP_Config(void)
{

	//1.开GPIOE端口时钟
	RCC->AHB1ENR |= 1<<4;
	//3.默认状态
	GPIOE->MODER   &= ~(3<<2*0);
	GPIOE->MODER   |=  (1<<2*0);
	GPIOE->OTYPER  &= ~(1<<0);
	GPIOE->OSPEEDR |= (3<<2*0);
	GPIOE->PUPDR   &= ~(3<<2*0);
}

void BEEP_On(void)
{
	GPIOE->ODR |=   (1<<0);//开
	//GPIO_SetBits(GPIOE, GPIO_Pin_0);
}

void BEEP_Off(void)
{
	GPIOE->ODR &= ~ (1<<0);//关
	//GPIO_ResetBits(GPIOE, GPIO_Pin_0);
}

void BEEP_Bi(uint8_t n)
{
	BEEP_On();
	Delay_ms(n);
	BEEP_Off();
	Delay_ms(n);
}
