/*
 * iwdg.c
 *
 *  Created on: 2024年7月5日
 *      Author: 23206
 */

#include "iwdg.h"

//启动看门狗 主要开始到计数  原理：给KR 写一个特殊值  0xCCCC;
void IWDG_Start(void)
{
	IWDG->KR = 0xCCCC;
}
//喂狗 作用：防止复位  由于喂狗会导致RLR的值重新加载到计数器 原理： 给KR写一个特殊值 0xAAAA;
void IWDG_FeedDog(void)
{
	IWDG->KR = 0xAAAA;
}

//关闭写保护 原理： 给KR 写入一个特殊值  0x5555;
void IWDG_Unlock(void)
{
	IWDG->KR = 0x5555;
}
//原理：给RLR 写一个值  写Val   注意事项：不能超过最大值 写之前要解锁
void IWDG_SetLoadVal(uint16_t Val)
{
	IWDG_Unlock();
	while(IWDG->SR & 1<<1);
	IWDG->RLR = Val-1;
}

//原理：gei PR写一个值 0_4 1_8 2_16 3_32 4_64 5_128 6_256 7_256   注意事项：不能超过最大值 写之前要解锁
void IWDG_SetPscVal(PSC_e psc)
{
	IWDG_Unlock();
	while(IWDG->SR & 1<<0);
	IWDG->PR |= psc;
}


void IWDG_Config(void)
{
	IWDG_SetPscVal(PSC_32);
	IWDG_SetLoadVal(4096);
	IWDG_Start();
	IWDG_FeedDog();
}
