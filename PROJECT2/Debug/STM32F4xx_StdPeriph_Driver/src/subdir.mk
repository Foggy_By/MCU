################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../STM32F4xx_StdPeriph_Driver/src/misc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.c \
../STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.c 

OBJS += \
./STM32F4xx_StdPeriph_Driver/src/misc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.o \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.o 

C_DEPS += \
./STM32F4xx_StdPeriph_Driver/src/misc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.d \
./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.d 


# Each subdirectory must supply rules for building sources it contributes
STM32F4xx_StdPeriph_Driver/src/%.o STM32F4xx_StdPeriph_Driver/src/%.su STM32F4xx_StdPeriph_Driver/src/%.cyclo: ../STM32F4xx_StdPeriph_Driver/src/%.c STM32F4xx_StdPeriph_Driver/src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -g3 -DDEBUG -DUSE_STDPERIPH_DRIVER -DSTM32F40_41xxx -DSTM32 -DSTM32F4 -DSTM32F407VGTx -c -I../Inc -I../STM32F4xx_StdPeriph_Driver/inc -I../CMSIS -O0 -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-STM32F4xx_StdPeriph_Driver-2f-src

clean-STM32F4xx_StdPeriph_Driver-2f-src:
	-$(RM) ./STM32F4xx_StdPeriph_Driver/src/misc.cyclo ./STM32F4xx_StdPeriph_Driver/src/misc.d ./STM32F4xx_StdPeriph_Driver/src/misc.o ./STM32F4xx_StdPeriph_Driver/src/misc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_adc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_can.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cec.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_crc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_aes.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_des.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_cryp_tdes.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dac.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dbgmcu.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dcmi.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dfsdm.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dma2d.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_dsi.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_exti.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_flash_ramfunc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fmpi2c.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_fsmc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_gpio.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_md5.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_hash_sha1.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_i2c.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_iwdg.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_lptim.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.o
	-$(RM) ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_ltdc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_pwr.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_qspi.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rcc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rng.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_rtc.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sai.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_sdio.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spdifrx.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_spi.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_syscfg.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_tim.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_usart.su ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.cyclo ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.d ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.o ./STM32F4xx_StdPeriph_Driver/src/stm32f4xx_wwdg.su

.PHONY: clean-STM32F4xx_StdPeriph_Driver-2f-src

