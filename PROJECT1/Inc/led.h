#ifndef _LED_H
#define _LED_H

#include "stm32f4xx.h"

//宏定义
#define LED1_ON (GPIOC->ODR &= ~(1 << 4))
#define LED2_ON (GPIOC->ODR &= ~(1 << 5))
#define LED3_ON (GPIOC->ODR &= ~(1 << 6))
#define LED4_ON (GPIOC->ODR &= ~(1 << 7))

#define LED1_OFF (GPIOC->ODR |= (1 << 4))
#define LED2_OFF (GPIOC->ODR |= (1 << 5))
#define LED3_OFF (GPIOC->ODR |= (1 << 6))
#define LED4_OFF (GPIOC->ODR |= (1 << 7))

#define LED1_OVERTURN (GPIOC->ODR ^= (1 << 4))
#define LED2_OVERTURN (GPIOC->ODR ^= (1 << 5))
#define LED3_OVERTURN (GPIOC->ODR ^= (1 << 6))
#define LED4_OVERTURN (GPIOC->ODR ^= (1 << 7))


//函数声明
u8 keyscan(void);
void Led_Init(void);
void Led_All_On(void);
void Led_All_Off(void);
void Led_On(char n);
void Led_Off(char n);
void Led_Control(char n,char c);
void Led_Key_Control(void);
void Led_Flash(char n);
void Led_All_Flash(void);
void Led_Water(void);
void Led_Horse(void);
void Led_Horse_Speed(u32 speed);
void Led_Breath(void);
void Led_Bright(char n,u32 ontime,u32 T);
void Led_Bright_Control(void);
void Led_Water_Control(void);
void Led_Water_Speed(u32 speed);
void Led_Water_Control(void);

#endif

