/*
 * iwdg.h
 *
 *  Created on: 2024年7月5日
 *      Author: 23206
 */

#ifndef IWDG_H_
#define IWDG_H_

#include "stm32f4xx.h"


typedef enum
{
	PSC_4   = 0,
	PSC_8   = 1,
	PSC_16  = 2,
	PSC_32  = 3,
	PSC_64  = 4,
	PSC_128 = 5,
	PSC_256 = 6,
}PSC_e;

void IWDG_Config(void);
#endif /* IWDG_H_ */
