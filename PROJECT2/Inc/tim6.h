
#ifndef _TIM6_H_
#define _TIM6_H_

#include "stm32f4xx.h"

//函数声明
void TIM6_delay1ms(void);
void TIM6_delay1us(void);
void TIM6_delay_ms(u32 n);
void TIM6_delay_us(u32 n);

#endif
