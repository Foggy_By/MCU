/*
 * adc1.h
 *
 *  Created on: 2024年7月4日
 *      Author: 23206
 */

#ifndef ADC1_H_
#define ADC1_H_

#include "stm32f4xx.h"

void ADC1_IN0_Config(void);
void ADC1_Config(void);
void ADC1_IN0_IN1_Config(void);
//uint16_t ADC1_GetVal(void);

void ADC_CHx_CHy_GetVal(void);

#endif /* ADC1_H_ */
