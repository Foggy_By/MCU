/*
 * systick.c
 *
 *  Created on: 2024年6月30日
 *      Author: 23206
 */


#include "systick.h"
#include "led.h"
#include "adc1.h"
#include <stdio.h>
//时钟频率是168MHz  暗含了bit2 = 1
//计数值是  168000 暗含了 LOAD = 168000 技术168000次  168000-1  递减到0
//定时器开关 bit0
//定时器中断 bit1
//void SYSTICK_Config(void)
//{
//	SysTick->LOAD = 168000-1;
//	SysTick->VAL  = 0;        //当前值寄存器 写什么都清0 0 - > 168000-1;
//
//	//SysTick->CTRL&= ~(1<<2)      //SysTick 0=外部时钟源(STCLK = 21MHz)
//	SysTick->CTRL|=  (1<<2);     //SysTick 1=内核时钟(FCLK = 168MHz)
//
//
//	SysTick->CTRL|=  (1<<1);     //SysTick 异常请求使能位
//	SysTick->CTRL|=  (1<<0);     //SysTick 定时器的使能位
//}



void MYSYS_Config(void)
{
	SysTick_CLKSourceConfig(SysTick_CLKSource_HCLK);
	SysTick_Config(168000);
}

uint32_t LED1_Time = 100;
//嘀嗒定时器中断服务函数
void SysTick_Handler(void)
{
	static uint32_t system_time = 0;
	if(SysTick->CTRL&1<<16)
	{
	    system_time++;//1ms时间到
		if(system_time%10 ==0)
		{
			IWDG_FeedDog();
			printf("WG le \r\n");
			//ADC_CHx_CHy_GetVal();
			// ADC1_GetVal();
			 //printf("ADC_VAL = %d \r\n",);
			//printf("system_time = %d\r\n",system_time);
			//printf("TIM7->CNT = %d\r\n",TIM7->CNT);
			//printf("TIM3->CNT = %d\r\n",TIM3->CNT);
		}
		if(LED1_Time>=1000)
			LED1_Time =1000;
		if(LED1_Time<=0)
			LED1_Time = 100;
		if(system_time%LED1_Time==0)
		{
			LED_Tog(1);

			//printf("+++LED1_Time = %d\r\n",LED1_Time);
			//printf("LED1 State is %d\r\n",(GPIOC->ODR&1<<4)>>4);
		}
	}
}
