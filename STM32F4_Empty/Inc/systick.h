/*
 * systick.h
 *
 *  Created on: 2024年6月30日
 *      Author: 23206
 */

#ifndef _SYSTICK_H_
#define _SYSTICK_H_



#include "stm32f4xx.h"


extern uint32_t LED1_Time ;



void SYSTICK_Config(void);
void MYSYS_Config(void);
#endif /* _SYSTICK_H_ */
