/*
 * tim7.c
 *
 *  Created on: Jul 1, 2024
 *      Author: 23206
 */

#include "tim7.h"

void TIM7_Config(void)
{
	//1.打开对应的时钟 上电
	RCC->APB1ENR |= 1<<5;
	//2.配置时基单元  决定时间
	TIM7->CNT = 0;      //计数值是0开始
	TIM7->PSC = 8400-1;   //1MHz
	TIM7->ARR = 50000-1;  //1000个数   1000 * 1US = 1MS

	TIM7->CR1 |=  (1<<7);    //缓冲模式
	TIM7->CR1 &= ~(1<<3);    //非单脉冲模式
	TIM7->CR1 &= ~(1<<2);
	TIM7->CR1 &= ~(1<<1);

	//使用UG位进行更新配置 软件更新
	TIM7->EGR = 1;
	TIM7->SR  = 0;

	TIM7->DIER |= 1<<0;//开中断开关
	NVIC_SetPriority(TIM7_IRQn, NVIC_EncodePriority(7-2,1,3));
	NVIC_EnableIRQ(TIM7_IRQn); //使能某个编号的中断

	TIM7->CR1 |=  (1<<0);    //使能运行
}


void TIM7_IRQHandler(void)
{
	//1.判断标志位
	if(TIM7->SR & 1<<0)
	{
		//2.清除标志
		TIM7->SR &= ~(1<<0);
		//printf("TIM7 5S IS OK\r\n");
	}
}
