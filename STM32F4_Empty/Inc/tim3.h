/*
 * tim3.h
 *
 *  Created on: 2024年7月2日
 *      Author: 23206
 */

#ifndef TIM3_H_
#define TIM3_H_

#include "stm32f4xx.h"


void TIM3_SetCCR1(uint16_t Val);
void TIM3_SetCCR2(uint16_t Val);
void TIM3_SetCCR3(uint16_t Val);
void TIM3_SetCCR4(uint16_t Val);
void TIM3_CH1_Config(void);
void TIM3_Config(void);

#endif /* TIM3_H_ */
