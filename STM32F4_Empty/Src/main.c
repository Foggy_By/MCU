#include <stdint.h>
#include "main.h"
#include <stdio.h>
#include <string.h>
#include "key.h"
#include "led.h"
#include "beep.h"
#include "delay.h"
#include "usart1.h"
#include "exti.h"
#include "systick.h"
#include "tim3.h"
#include "tim6.h"
#include "tim7.h"

//#if !defined(__SOFT_FP__) && defined(__ARM_FP)
//  #warning "FPU is not initialized, but the project is compiling for an FPU. Please initialize the FPU before use."
//#endif

uint16_t Val      = 100;
uint8_t Recv_Data = 0;//全局变量
uint8_t Key_Num   = 0;//全局变量

int main(void)
{
	//5 -> XX.YY 抢占.次级 00 01 10 11 0 1 2 3 次级 0 1 2 3
	//0到3选一个抢占 0到3选一个 C41 * C41
	//NVIC_SetPriorityGrouping(7-2); // 5分组的一个结果  2代表x位数是两位  总共4位 2位 //5 101
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);//STM32 官方实现的 库函数
	USART1_Config(115200);
	USART2_Config(115200);
	KEY_Config();
	LED_Config();
	BEEP_Config();
	//EXTI_Config();
	for(uint8_t i = 1;i < 5; i++)
	{
		LED_Ctrl(i, 0);
		BEEP_Bi(4);
	}
//	SYSTICK_Config();
	MYSYS_Config();
	TIM7_Config();
	TIM3_CH1_Config();
	TIM5_CH1_Config();
	ADC1_IN0_Config();
	ADC1_IN0_IN1_Config();
	//IWDG_Config();
	WWDG_Config(0x7F);
	USART_SendString("hello\r\nworld\r\n");
	uint8_t group = (SCB->AIRCR & 7<<8)>>8;
	printf("Buid time%s\r\n",__TIME__);
	printf("group num is %d\r\n",group);
    while(1)
    {

        for(Val = 100;Val<20000;Val+=20)
        {
        	TIM3_SetCCR1(255);
        	TIM6_Delay_NMS(20);
        }
    	if(A_Flag && B_Flag && C_Flag == 2)
    	{
    		printf("+1\r\n");
    		A_Flag = 0;
    		B_Flag = 0;
    		C_Flag = 0;
    		BEEP_Bi(5);
    	}
    	if(A_Flag && B_Flag && C_Flag == 1)
    	{
    		printf("-1\r\n");
    		A_Flag = 0;
    		B_Flag = 0;
    		C_Flag = 0;
    	}
    	Key_Num = KEY_Check();
    	switch (Key_Num)
    	{
			case 1: USART2_SendString("Open");
			break;
			case 2:	USART2_SendString("Close");
			break;
			case 3:
				printf("LED1_Time = %d\r\n",LED1_Time);
				LED1_Time = LED1_Time + 100;
				Val+=500;

			break;
			case 4:
				printf("LED1_Time = %d\r\n",LED1_Time);
				LED1_Time = LED1_Time - 100;
				Val-=500;
			break;
			default:
				break;
		}
    	if(USART1_Recv.Recv_Flag==1)//串口1收到是什么   打印 同时转发给串口2
    	{
    		printf("U1 Recv info is %s\r\n",USART1_Recv.Recv_Buff);//收到的信息 打印出来看一下是什么
    		USART2_SendString(USART1_Recv.Recv_Buff);//1
    		USART1_Recv.Recv_Flag = 0;
    	}
//    	if(USART2_Recv.Recv_Flag==1)//串口2 收到的是什么 打印
//    	{
//    		printf("U2 Recv info is %s\r\n",USART2_Recv.Recv_Buff);
//    		if(strcmp(USART2_Recv.Recv_Buff,"Close")==0)
//    		{
//    			BEEP_Off();
//    		}
//    		else if(strcmp(USART2_Recv.Recv_Buff,"Open")==0)
//    		{
//    			BEEP_On();
//    		}
//    		USART2_Recv.Recv_Flag = 0;
//    	}
    }
}



