#ifndef _DELAY_H_
#define _DELAY_H_

#include "stm32f4xx.h"

//1s = 1000ms = 1000 000us
//Hz  KHz  MHz
//1MHz 计数一个数 1us
//168MHz 计数168000 000 个数 需要1s
//168MHz 计数168000  个数    需要1ms
//168MHz 计数168  个数       需要1us

void Delay_us(void);
void Delay_ms(uint32_t n);


#endif
