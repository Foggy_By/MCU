
#ifndef _EXTI_H_
#define _EXTI_H_

#include "stm32f4xx.h"
#include <stdio.h>

//外部变量声明
extern u8 Aflag;
extern u8 Bflag;
extern u32 count;

//函数声明
void Exti_Init(void);

#endif
