#include "wwdg.h"
#include <stdio.h>

//函数是寄存器的操作封装
//喂狗
void WWDG_WG(void)
{
	WWDG->CR|= 0x5F;
}
//设置分频
void WWDG_SetPsc(void)
{
	WWDG->CFR |= 0x3<<7;
}
//设置窗口
void WWDG_SetWindow(uint8_t val)
{
	WWDG->CFR |= 0x6F;
}
//启动看门狗
void WWDG_Start(void)
{
	WWDG->CR |= 1<<7;
}
//开启看门狗中断
void WWDG_IRQEn(void)
{
	WWDG->CFR |= 1<<9;
}
//窗口看门狗配置
void WWDG_Config(uint8_t val)
{
	RCC->APB1ENR |= 1<<11;
	WWDG_SetPsc();
	WWDG_SetWindow(val);
	WWDG_IRQEn();
	NVIC_SetPriority(WWDG_IRQn,NVIC_EncodePriority(7-2,1,2));
	NVIC_EnableIRQ(WWDG_IRQn);
	WWDG_Start();
}

void WWDG_IRQHandler(void)
{
	if(WWDG->SR & 1<<0)
	{
	   WWDG->SR &=~(1<<0);
	   WWDG_WG();
	   printf("Window Wg l \r\n");
	}
}
