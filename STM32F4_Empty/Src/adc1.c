/*
 * adc1.c
 *
 *  Created on: 2024年7月4日
 *      Author: 23206
 */

#include "adc1.h"



//PA0的模拟模式

void ADC1_Config(void)
{

	//ADC1 使能在RCC_APB2ENR
	RCC->APB2ENR |= 1<<8;      //打卡ADC1的时钟  84MHz  分频

	ADC->CCR &=  ~(0x3<<16);   //00
	ADC->CCR |=   (0x1<<16);   //01   4分频    21MHz
	ADC->CCR &=  ~(0x1F<<0);    //独立模式


    ADC1->CR1   &= ~(0x3<<24); //00  分辨率12
    ADC1->CR1   |=  (0x1<<8);  //扫描位  打开



	ADC1->SMPR2 &= ~(0x7<<3*0);//000 =  3 + 12   15/21 = us
	ADC1->SMPR2 &= ~(0x7<<3*1);//000 =  3 + 12   15/21 = us

    ADC1->CR2   &= ~(0x1<<11); //右对齐
    ADC1->CR2   |=  (1<<10)  ; //选择每个通道转换完成都可以触发EOC 标志 SR  EOCS ==1

    //规则里选一个或者多个
	ADC1->SQR1 &= ~(0xF<<20);     //规则里有1个通道
	ADC1->SQR1 |=  (0x1<<20);     //规则里有2个通道

	ADC1->SQR3 &= ~(0x1F<<5*0);  //00000   第1次转换通道00000
	ADC1->SQR3 |=  (0x0<<5*0);   //00000   第1次转换谁  PA0

	ADC1->SQR3 &= ~(0x1F<<5*1);  //00000   第2次转换通道00000
	ADC1->SQR3 |=  (0x1<<5*1);   //00000   第2次转换谁  PA1

	ADC1->CR2 |=  (1<<1);//连续
	ADC1->CR2 |= (0x1<<0);       //AD ON
	ADC1->CR2 |= 1<<30;         //开启转换
}

void ADC1_IN0_Config(void)
{
	RCC->AHB1ENR |= 1<<0;

	GPIOA->MODER &= ~(0x3<<2*0);
	GPIOA->MODER |=  (0x3<<2*0);//模拟模式 配置

}

void ADC1_IN1_Config(void)
{
	RCC->AHB1ENR |= 1<<0;

	GPIOA->MODER &= ~(0x3<<2*1);
	GPIOA->MODER |=  (0x3<<2*1);//模拟模式 配置
}


void ADC1_IN0_IN1_Config(void)
{
	ADC1_IN0_Config();
	ADC1_IN1_Config();
	ADC1_Config();
}
//A1的值  A0的值
//uint16_t ADC1_GetVal(void)
//{
//	static uint16_t ADC_Data[2]={0};
//	for(uint8_t i = 0;i<2;i++)
//	{
//		ADC1->CR2 |= 1<<30;         //开启转换
//		while(!(ADC1->SR & 1<<1));  //（配置时间+12）周期
//		ADC_Data[i] = ADC1->DR;
//		printf("ADC_Data[%d]= %d\r\n",i,ADC_Data[i]);
//	}
//	return 0;            //返回采样结果
//}
uint16_t ADC_Data[2]={0};
//函数返回值是ADC结果
void ADC_CHx_CHy_GetVal(void)
{
	static uint8_t i = 0;
	for(i=0;i<2;i++)
	{
	   ADC1->CR2    |=  (1<<30);        //开启转换 软件开启转换  SWSTART
	   while(!(ADC1->SR & 1<<1));       //EOC置位代表转换完成数据 一次一个数据
	   ADC_Data[i]= (ADC1->DR);
	   printf("ADC_Data[%d]= %d\r\n",i,ADC_Data[i]);
	}
}


