#ifndef _WWDG_H_
#define _WWDG_H_

#include "stm32f4xx.h"

void WWDG_WG(void);

void WWDG_Config(uint8_t val);

#endif
