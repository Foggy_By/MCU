#ifndef _LED_H_
#define _LED_H_

#include "stm32f4xx.h"
//#include <stdint.h>

void LED_Config(void);
void LED_Ctrl(uint8_t led_num,uint8_t led_sta);

void LED_Tog(uint8_t led_num);

#endif
