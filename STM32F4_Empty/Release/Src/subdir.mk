################################################################################
# Automatically-generated file. Do not edit!
# Toolchain: GNU Tools for STM32 (10.3-2021.10)
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
C_SRCS += \
../Src/beep.c \
../Src/delay.c \
../Src/exti.c \
../Src/key.c \
../Src/led.c \
../Src/main.c \
../Src/stm32f4xx_it.c \
../Src/syscalls.c \
../Src/sysmem.c \
../Src/system_stm32f4xx.c \
../Src/systick.c \
../Src/tim3.c \
../Src/tim6.c \
../Src/tim7.c \
../Src/usart1.c 

OBJS += \
./Src/beep.o \
./Src/delay.o \
./Src/exti.o \
./Src/key.o \
./Src/led.o \
./Src/main.o \
./Src/stm32f4xx_it.o \
./Src/syscalls.o \
./Src/sysmem.o \
./Src/system_stm32f4xx.o \
./Src/systick.o \
./Src/tim3.o \
./Src/tim6.o \
./Src/tim7.o \
./Src/usart1.o 

C_DEPS += \
./Src/beep.d \
./Src/delay.d \
./Src/exti.d \
./Src/key.d \
./Src/led.d \
./Src/main.d \
./Src/stm32f4xx_it.d \
./Src/syscalls.d \
./Src/sysmem.d \
./Src/system_stm32f4xx.d \
./Src/systick.d \
./Src/tim3.d \
./Src/tim6.d \
./Src/tim7.d \
./Src/usart1.d 


# Each subdirectory must supply rules for building sources it contributes
Src/%.o Src/%.su Src/%.cyclo: ../Src/%.c Src/subdir.mk
	arm-none-eabi-gcc "$<" -mcpu=cortex-m4 -std=gnu11 -DSTM32 -DSTM32F4 -DSTM32F407VGTx -c -I../Inc -Os -ffunction-sections -fdata-sections -Wall -fstack-usage -fcyclomatic-complexity -MMD -MP -MF"$(@:%.o=%.d)" -MT"$@" --specs=nano.specs -mfpu=fpv4-sp-d16 -mfloat-abi=hard -mthumb -o "$@"

clean: clean-Src

clean-Src:
	-$(RM) ./Src/beep.cyclo ./Src/beep.d ./Src/beep.o ./Src/beep.su ./Src/delay.cyclo ./Src/delay.d ./Src/delay.o ./Src/delay.su ./Src/exti.cyclo ./Src/exti.d ./Src/exti.o ./Src/exti.su ./Src/key.cyclo ./Src/key.d ./Src/key.o ./Src/key.su ./Src/led.cyclo ./Src/led.d ./Src/led.o ./Src/led.su ./Src/main.cyclo ./Src/main.d ./Src/main.o ./Src/main.su ./Src/stm32f4xx_it.cyclo ./Src/stm32f4xx_it.d ./Src/stm32f4xx_it.o ./Src/stm32f4xx_it.su ./Src/syscalls.cyclo ./Src/syscalls.d ./Src/syscalls.o ./Src/syscalls.su ./Src/sysmem.cyclo ./Src/sysmem.d ./Src/sysmem.o ./Src/sysmem.su ./Src/system_stm32f4xx.cyclo ./Src/system_stm32f4xx.d ./Src/system_stm32f4xx.o ./Src/system_stm32f4xx.su ./Src/systick.cyclo ./Src/systick.d ./Src/systick.o ./Src/systick.su ./Src/tim3.cyclo ./Src/tim3.d ./Src/tim3.o ./Src/tim3.su ./Src/tim6.cyclo ./Src/tim6.d ./Src/tim6.o ./Src/tim6.su ./Src/tim7.cyclo ./Src/tim7.d ./Src/tim7.o ./Src/tim7.su ./Src/usart1.cyclo ./Src/usart1.d ./Src/usart1.o ./Src/usart1.su

.PHONY: clean-Src

