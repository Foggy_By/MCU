
#ifndef _USART1_H_
#define _USART1_H_

#include "stm32f4xx.h"
#include <stdio.h>
#include "led.h"

//宏定义
#define datalen 100//接收到的数据最长为100字节

typedef struct
{
	u8 recvdata[datalen];//接收到的信息（数组）
	u8 count;//数组下标
	u8 flag;//标志位，标志信息是否完全接收（包括'\0'）
} USART_STRUCT;

//外部变量声明（需包含本文件才可使用这些变量）
extern USART_STRUCT usart1_struct;
extern USART_STRUCT usart2_struct;

//函数声明
void USART1_Init(void);
void USART1_SendByte(u8 bitdata);
void USART1_SendString(u8* pstr);
u8 USART1_ReceiveByte(void);

#endif

