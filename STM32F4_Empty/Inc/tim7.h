/*
 * tim7.h
 *
 *  Created on: Jul 1, 2024
 *      Author: 23206
 */

#ifndef TIM7_H_
#define TIM7_H_

#include "stm32f4xx.h"
void TIM7_Config(void);
#endif /* TIM7_H_ */
