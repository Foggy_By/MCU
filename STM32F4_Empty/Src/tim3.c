/*
 * tim3.c
 *
 *  Created on: 2024年7月2日
 *      Author: 23206
 */


#include "tim3.h"
#include <stdio.h>

//PC6 - TIM3_CH1 - AF2
//PC7 - TIM3_CH2 - AF2
//PB0 - TIM3_CH3 - AF2
//PB1 - TIM3_CH4 - AF2

//

void TIM3_CH1_Config(void)
{
	//1.开时钟
	RCC->AHB1ENR |= 1<<2;          // 	C端口时钟
	RCC->AHB1ENR |= 1<<1;          // 	B端口时钟

	GPIOC->MODER &= ~(0x3<<6*2);   //00  浮空模式
	GPIOC->MODER |=  (0x2<<6*2);   //10  复用模式
	GPIOC->OTYPER &= ~(0x1<<6*1);   //		清零
	GPIOC->OSPEEDR&= ~(0x3<<6*2);   //		清零
	GPIOC->PUPDR  &= ~(0x3<<6*2);
	GPIOC->AFR[0]&= ~(0xF<<6*4);//把AF高位寄存器的 设置成0000
	GPIOC->AFR[0]|=  (0x2<<6*4);//把AF高位寄存器的24 25 26 27 设置从0010

	GPIOC->MODER  &= ~(0x3<<7*2);
	GPIOC->MODER  |=  (0x2<<7*2);
	GPIOC->OTYPER &= ~(0x1<<7*1);
	GPIOC->OSPEEDR&= ~(0x3<<7*2);
	GPIOC->PUPDR  &= ~(0x3<<7*2);
	GPIOC->AFR[0] &= ~(0xF<<7*4);
	GPIOC->AFR[0] |=  (0x2<<7*4);

	GPIOB->MODER  &= ~(0x3<<0*2);
	GPIOB->MODER  |=  (0x2<<0*2);
	GPIOB->OTYPER &= ~(0x1<<0*1);
	GPIOB->OSPEEDR&= ~(0x3<<0*2);
	GPIOB->PUPDR  &= ~(0x3<<0*2);
	GPIOB->AFR[0] &= ~(0xF<<0*4);
	GPIOB->AFR[0] |=  (0x2<<0*4);

	GPIOB->MODER  &= ~(0x3<<1*2);
	GPIOB->MODER  |=  (0x2<<1*2);
	GPIOB->OTYPER &= ~(0x1<<1*1);
	GPIOB->OSPEEDR&= ~(0x3<<1*2);
	GPIOB->PUPDR  &= ~(0x3<<1*2);
	GPIOB->AFR[0] &= ~(0xF<<1*4);
	GPIOB->AFR[0] |=  (0x2<<1*4);

	TIM3_Config();
}
//基本定时器的部分功能
void TIM3_Config(void)
{
	//1.开时钟
	RCC->APB1ENR |= 1<<1;
	//2.时基单元
	TIM3->CNT = 0;
	TIM3->PSC = 84-1;
	TIM3->ARR = 256-1;  //256us

    TIM3->CR1 |=  (1<<7);
    TIM3->CR1 &= ~((1<<3)|(1<<2)|(1<<1));


	TIM3->EGR |=  (1<<0);  //软件触发影子寄存器
    TIM3->SR  &= ~(1<<0);

    //进行一个输出比较的配置
    //有效电平是什么
    //无效电平是什么
    //PWM1进行比较
    //PWM2进行比较
    //比较使能打开   8个bit+4bit
    //进行一个输入捕获的配置
    //只要把这个12个比特写好


     TIM3->CCMR1 &= ~(0xFF<<8*0);
     TIM3->CCMR1 |=  (0x6 <<4);   //110
     TIM3->CCER  &= ~(0xF<<4*0);
     TIM3->CCER  |=  (1<<4*0);

     TIM3->CCMR1 &= ~(0xFF<<8*1);
     TIM3->CCMR1 |=  (0x6 <<4+8); //110
     TIM3->CCER  &= ~(0xF<<4*1);
     TIM3->CCER  |=  (1<<4*1);

     TIM3->CCMR2 &= ~(0xFF<<8*0);
     TIM3->CCMR2 |=  (0x6 <<4);   //110
     TIM3->CCER  &= ~(0xF<<4*2);
     TIM3->CCER  |=  (1<<4*2);

     TIM3->CCMR2 &= ~(0xFF<<8*1);
     TIM3->CCMR2 |=  (0x6 <<4+8); //110
     TIM3->CCER  &= ~(0xF<<4*3);
     TIM3->CCER  |=  (1<<4*3);


//    TIM3->DIER |= 1<<0;
//    NVIC_SetPriority(TIM3_IRQn, NVIC_EncodePriority(7-2,1,3));
//    NVIC_EnableIRQ(TIM3_IRQn); //使能某个编号的中断

    TIM3->CR1 |= 1<<0;
}

void TIM3_SetCCR1(uint16_t Val)
{
	TIM3->CCR1 = Val;
//	printf("TIM->Val  = %d\r\n",Val);
//	printf("TIM3_CNT  = %d\r\n",TIM3->CNT);
//	printf("TIM3_CCR1 = %d\r\n",TIM3->CCR1);
}
void TIM3_SetCCR2(uint16_t Val)
{
	TIM3->CCR2 = Val;
//	printf("TIM->Val  = %d\r\n",Val);
//	printf("TIM3_CNT  = %d\r\n",TIM3->CNT);
//	printf("TIM3_CCR1 = %d\r\n",TIM3->CCR1);
}
void TIM3_SetCCR3(uint16_t Val)
{
	TIM3->CCR3 = Val;
//	printf("TIM->Val  = %d\r\n",Val);
//	printf("TIM3_CNT  = %d\r\n",TIM3->CNT);
//	printf("TIM3_CCR1 = %d\r\n",TIM3->CCR1);
}
void TIM3_SetCCR4(uint16_t Val)
{
	TIM3->CCR4 = Val;
//	printf("TIM->Val  = %d\r\n",Val);
//	printf("TIM3_CNT  = %d\r\n",TIM3->CNT);
//	printf("TIM3_CCR1 = %d\r\n",TIM3->CCR1);
}

void RGB_SetColor(uint8_t Red,uint8_t Green,uint8_t Blue)
{
	TIM3_SetCCR1(Red);
	TIM3_SetCCR2(Green);
	TIM3_SetCCR3(Blue);
}
//Speed范围 0 - 100
//占空比100% 100
//100/256
void Motor_SetSpeed(uint8_t Speed)
{
	//y=(x,a,b,c,d);//x 介于 a和b 之间 y 介于c和d之间
	TIM3_SetCCR4(10*Speed);
}


////中断服务函数的特点
//void TIM3_IRQHandler(void)
//{
//	//1.判断标志位
//	if(TIM3->SR & 1<<0)
//	{
//		//2.清除标志
//		TIM3->SR &= ~(1<<0);
//		printf("TIM3 5S IS OK\r\n");
//	}
//}



