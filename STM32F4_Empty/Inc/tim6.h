/*
 * tim6.h
 *
 *  Created on: Jul 1, 2024
 *      Author: 23206
 */

#ifndef TIM6_H_
#define TIM6_H_

#include "stm32f4xx.h"

void TIM6_Delay_1MS(void);
void TIM6_Delay_NMS(uint16_t N);
void TIM6_Delay_1S(void);
void TIM6_Delay_500MS(void);
#endif /* TIM6_H_ */
