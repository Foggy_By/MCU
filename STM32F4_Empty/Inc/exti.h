/*
 * exti.h
 *
 *  Created on: 2024年6月28日
 *      Author: 23206
 */

#ifndef EXTI_H_
#define EXTI_H_

#include "stm32f4xx.h"

extern uint8_t A_Flag ;
extern uint8_t B_Flag ;
extern uint8_t C_Flag ;

void EXTI_Config(void);
#endif /* EXTI_H_ */


