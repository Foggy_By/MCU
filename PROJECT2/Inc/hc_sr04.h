
#ifndef _HC_SR04_H_
#define _HC_SR04_H_

#include "stm32f4xx.h"
#include <stdio.h>
#include "tim6.h"

//函数声明
void Ultrasound_Init(void);
void Ultrasound_Measure(void);

#endif
