#ifndef _BEEP_H_
#define _BEEP_H_

#include "stm32f4xx.h"


void BEEP_Config(void);
void BEEP_On(void);
void BEEP_Off(void);
void BEEP_Bi(uint8_t n);

#endif
