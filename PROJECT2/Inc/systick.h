
#ifndef _SYSTICK_H_
#define _SYSTICK_H_

#include "stm32f4xx.h"
#include <stdio.h>
#include <time.h>
#include "led.h"
#include "adc1.h"
#include "iwdg.h"

//外部变量
extern s32 T1,T2;

//函数声明
void Systick_Init(void);

#endif
