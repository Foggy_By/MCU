#ifndef _MAIN_H
#define _MAIN_H

#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "stm32f4xx.h"

#include "delay.h"
#include "led.h"
#include "beep.h"
#include "key.h"
#include "usart1.h"
#include "usart2.h"
#include "exti.h"
#include "systick.h"
#include "tim6.h"
#include "tim7.h"
#include "tim3.h"
#include "tim5.h"
#include "adc1.h"
#include "hc_sr04.h"
#include "iwdg.h"
#include "wwdg.h"


#endif

