
#ifndef _ADC1_H_
#define _ADC1_H_

#include "stm32f4xx.h"
#include <stdio.h>

//宏定义
#define SCAN_FLAG 	1//扫描
#define CONT_FLAG 	1//连续采样
#define EOCIE_FLAG 	1//EOC中断

//外部变量声明
extern u16 ADC1_Data[2];

//函数声明
void ADC1_Init(void);
void ADC1_Getdata(void);

#endif
