/*
 * tim5.h
 *
 *  Created on: Jul 3, 2024
 *      Author: 23206
 */

#ifndef TIM5_H_
#define TIM5_H_

#include "stm32f4xx.h"

void TIM5_CH1_Config(void);
void TIM5_Config(void);
#endif /* TIM5_H_ */
