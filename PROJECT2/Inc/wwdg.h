
#ifndef _WWDG_H_
#define _WWDG_H_

#include "stm32f4xx.h"

//定义结构体和枚举
typedef enum
{
	PSC_1 = 0,
	PSC_2 = 1,
	PSC_4 = 2,
	PSC_8 = 3
} WWDG_enum;

//函数声明
void WWDG_Init(void);

#endif
