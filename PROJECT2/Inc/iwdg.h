
#ifndef _IWDG_H_
#define _IWDG_H_

#include "stm32f4xx.h"

//定义结构体和枚举
typedef enum
{
	PSC4 				= 0,
	PSC8 				= 1,
	PSC16 				= 2,
	PSC32 				= 3,
	PSC64 				= 4,
	PSC128 			= 5,
	PSC256 			= 6,
	PSC256_copy 	= 7,
} PSC_enum;

//函数声明
void IWDG_Feed(void);
void IWDG_Init(void);

#endif
