#ifndef _BEEP_H
#define _BEEP_H

#include "stm32f4xx.h"
#include "tim6.h"

//宏定义
#define BEEP_ON					(GPIOE->ODR |= (1<<0))
#define BEEP_OFF 				(GPIOE->ODR &= ~(1<<0))
#define BEEP_OVERTURN 	(GPIOE->ODR ^= (1<<0))


//函数声明
void Beep_Init(void);
void Beep_ms(u32 n);
void Beep_Flash(void);
void Beep_Lowvoice(void);



#endif

