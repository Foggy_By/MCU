
#ifndef _TIM3_H_
#define _TIM3_H_

#include "stm32f4xx.h"
#include <stdio.h>

//函数声明
void TIM3_CH_Init(void);
void TIM3_Init(void);
void TIM3_SetCCR1(u16 value);
void TIM3_SetCCR2(u16 value);
void TIM3_SetCCR3(u16 value);

#endif
