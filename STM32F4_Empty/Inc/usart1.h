/*
 * usart1.h
 *
 *  Created on: 2024年6月25日
 *      Author: 23206
 */

#ifndef _USART1_H_
#define _USART1_H_

#include "stm32f4xx.h"


#define BUFF_SIZE 256

//用户自定义数据类型
typedef struct
{
	uint8_t Recv_Buff[BUFF_SIZE];
	uint8_t Recv_Count;
	uint8_t Recv_Flag;
}USART_Recv_t;

extern USART_Recv_t  USART1_Recv;
extern USART_Recv_t  USART2_Recv;//定义一个结构体变量 开辟空间
//extern uint8_t Recv_Buff[BUFF_SIZE]; //声明的变量 可以在其他文件使用   其他要用的文件必须include
//extern uint8_t Recv_Count;
//extern uint8_t Recv_Flag;


void USART1_Config(uint32_t bps);
void USART2_Config(uint32_t bps);
void USART_SendByte(uint8_t Byte);
uint8_t USART_RecvByte(void);
void USART_SendString(uint8_t *pstr);

void USART2_SendByte(uint8_t Byte);
void USART2_SendString(uint8_t *pstr);

#endif /* _USART1_H_ */


