
#ifndef _USART2_H_
#define _USART2_H_

#include "stm32f4xx.h"
#include <stdio.h>
#include "usart1.h"

//宏定义



//函数声明
void USART2_Init(void);
void USART2_SendByte(u8 bitdata);
void USART2_SendString(u8* pstr);
u8 USART2_ReceiveByte(void);

#endif

