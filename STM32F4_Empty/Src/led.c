#include "led.h"
#include <stdio.h>
#include "main.h"
/*
 * 函数名称：LED_Config
 * 函数参数：无
 * 函数返回：无
 * 函数作用：配置PC4 PC5 PC6 PC7 推挽输出功能
 * RCC AHB1 外设时钟使能寄存器 (RCC_AHB1ENR)
 * 位 2 GPIOCEN：IO 端口 C 时钟使能 (IO port C clock enable)由软件置 1 和清零。
 * 0：禁止 IO 端口 C 时钟
 * 1：使能 IO 端口 C 时钟
 * */
void LED_Config(void)
{
#if 0
	//1.开GPIOC端口时钟
	RCC->AHB1ENR |= 1<<2;
	//2.配置相关寄存器

	//3.默认状态
	GPIOC->MODER   &= ~(3<<2*4);
	GPIOC->MODER   |=  (1<<2*4);
	GPIOC->OTYPER  &= ~(1<<4);
	GPIOC->OSPEEDR |= (3<<2*4);
	GPIOC->PUPDR   &= ~(3<<2*4);

	GPIOC->MODER  &= ~(3<<2*5);
	GPIOC->MODER  |=  (1<<2*5);
	GPIOC->OTYPER &= ~(1<<5);
	GPIOC->OSPEEDR |= (3<<2*5);
	GPIOC->PUPDR   &= ~(3<<2*5);

	GPIOC->MODER  &= ~(3<<2*6);
	GPIOC->MODER  |=  (1<<2*6);
	GPIOC->OTYPER &= ~(1<<6);
	GPIOC->OSPEEDR |= (3<<2*6);
	GPIOC->PUPDR   &= ~(3<<2*6);

	GPIOC->MODER  &= ~(3<<2*7);
	GPIOC->MODER  |=  (1<<2*7);
	GPIOC->OTYPER &= ~(1<<7);
	GPIOC->OSPEEDR |= (3<<2*7);
	GPIOC->PUPDR   &= ~(3<<2*7);

	GPIOC->ODR |= 0xF<<4;
#endif



}
/*
 * 函数名字：LED_Ctrl
 * 函数参数：uint8_t led_num 第几个灯 unit8_t led_sta 1代表开 0代表关
 * 函数返回：无
 * 函数作用：开指定的灯或者关指定的灯
 * */
void LED_Ctrl(uint8_t led_num,uint8_t led_sta)
{
	if(led_sta)
	{
		switch(led_num)
		{
			case 1:GPIOC->ODR &= ~(1<<4);break;
			case 2:GPIOC->ODR &= ~(1<<5);break;
			case 3:GPIOC->ODR &= ~(1<<6);break;
			case 4:GPIOC->ODR &= ~(1<<7);break;
		}
	}
	else
	{
		switch(led_num)
		{
			case 1:GPIOC->ODR |= (1<<4);break;
			case 2:GPIOC->ODR |= (1<<5);break;
			case 3:GPIOC->ODR |= (1<<6);break;
			case 4:GPIOC->ODR |= (1<<7);break;
		}
	}
}


void LED_Tog(uint8_t led_num)
{
	switch(led_num)
	{
		case 1:GPIOC->ODR ^= (1<<4);break;
		case 2:GPIOC->ODR ^= (1<<5);break;
		case 3:GPIOC->ODR ^= (1<<6);break;
		case 4:GPIOC->ODR ^= (1<<7);break;
	}
}





