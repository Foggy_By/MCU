/*
 * tim6.c
 *
 *  Created on: Jul 1, 2024
 *      Author: 23206
 */

#include "tim6.h"

void TIM6_Delay_1MS(void)
{
	//1.打开对应的时钟 上电
	RCC->APB1ENR |= 1<<4;
	//2.配置时基单元  决定时间
	TIM6->CNT = 0;      //计数值是0开始
	TIM6->PSC = 84-1;   //1MHz
	TIM6->ARR = 1000-1; //1000个数   1000 * 1US = 1MS

	//控制定时器开始工作 RUN
	TIM6->CR1 |= 1<<0;          //计数器开关
	TIM6->SR = 0;
	while(!(TIM6->SR & 1<<0));  //等待标志位置1 //等待计数完成标志置1
	TIM6->CR1 &= ~(1<<0);       //计数器开关
}


void TIM6_Delay_NMS(uint16_t N)
{
	RCC->APB1ENR |= 1<<4;
	TIM6->CNT = 0;      //计数值是0开始
	TIM6->PSC = 8400-1;   //1MHz
	TIM6->ARR = N*10-1; //1000个数   1000 * 1US = 1MS

	TIM6->CR1 |= 1<<0;          //计数器开关
	TIM6->SR = 0;
	while(!(TIM6->SR & 1<<0));  //等待标志位置1
	TIM6->CR1 &= ~(1<<0);       //计数器开关
}



void TIM6_Delay_1S(void)
{
	RCC->APB1ENR |= 1<<4;
	TIM6->CNT = 0;      //计数值是0开始
	TIM6->PSC = 8400-1;   //1MHz
	TIM6->ARR = 10000-1; //1000个数   1000 * 1US = 1MS

	TIM6->CR1 |= 1<<0;          //计数器开关
	TIM6->SR = 0;
	while(!(TIM6->SR & 1<<0));  //等待标志位置1
	TIM6->CR1 &= ~(1<<0);       //计数器开关
}



void TIM6_Delay_500MS(void)
{
	RCC->APB1ENR |= 1<<4;
	TIM6->CNT = 0;      //计数值是0开始
	TIM6->PSC = 8400-1;   //1MHz
	TIM6->ARR = 5000-1; //1000个数   1000 * 1US = 1MS

	TIM6->CR1 |= 1<<0;          //计数器开关
	TIM6->SR = 0;
	while(!(TIM6->SR & 1<<0));  //等待标志位置1
	TIM6->CR1 &= ~(1<<0);       //计数器开关
}
