#ifndef _KEY_H_
#define _KEY_H_

#include "stm32f4xx.h"

#define  REG_CODE_EN  1
#if REG_CODE_EN == 1

#define KEY1  (GPIOA->IDR & 1<<0)
#define KEY2 !(GPIOE->IDR & 1<<2)
#define KEY3 !(GPIOE->IDR & 1<<3)
#define KEY4 !(GPIOE->IDR & 1<<4)
#else
#define KEY1   GPIO_ReadInputDataBit(GPIOA,GPIO_Pin_0)
#define KEY2  !GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_2)
#define KEY3  !GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_3)
#define KEY4  !GPIO_ReadInputDataBit(GPIOE,GPIO_Pin_4)
#endif








void KEY_Config(void);
uint8_t KEY_Check(void);

#endif
